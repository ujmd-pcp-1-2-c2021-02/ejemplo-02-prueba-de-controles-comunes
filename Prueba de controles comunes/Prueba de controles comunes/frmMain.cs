﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prueba_de_controles_comunes
{
    public partial class frmMain : Form
    {
        const string user = "arturonvz";
        const string pwd = "asdfasdf";

        public frmMain()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text == user)
            {
                if (txtContraseña.Text == pwd)
                {
                    if (rbtnAlumno.Checked == true)
                    {
                        MessageBox.Show("El alumno " + txtUsuario.Text + 
                            ", de edad " + nudEdad.Value +  " años, ha iniciado sesión correctamente.");
                    }
                    else if (rbtnDocente.Checked == true)
                    {
                        MessageBox.Show("El docente " + txtUsuario.Text +
                            ", de edad " + nudEdad.Value + " años, ha iniciado sesión correctamente.");
                    }
                }
                else
                {
                    MessageBox.Show("Contraseña incorrecta.");
                }
            }
            else
            {
                MessageBox.Show("Usuario incorrecto.");
            }
        }

        private void chkMostrar_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMostrar.Checked == true)
            {
                txtContraseña.PasswordChar = '\0';
            }
            else
            {
                txtContraseña.PasswordChar = '*';
            }
        }
    }
}
